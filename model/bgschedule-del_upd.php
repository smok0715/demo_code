<?php
include_once('../config/db.php');
if(!isset($_SESSION['admin_user']['id'])) func::alertMsg('請登入', 'index.php' , true);   

$Ground_name = $_GET['Ground_name'];
$rand = $_GET['rand'];
 
$admin = $_SESSION['admin_user']['id'];
$ip = func::getUserIP();
$this_page = 'model/bgschedule-del_upd.php'; 

	if($rand)
	{
		$sql = "UPDATE ballground_schedule_limited SET schedule_is_del = 1, del_mark = '1'  WHERE rand =? AND del_mark = '0'; ";
		$updated = func::excSQLwithParam('update', $sql, array($rand), false, $PDOLink);
 
		$sql_ = "SELECT Address_MAC, meter_id, DATE_FORMAT(Start_date,'%Y-%m-%d') AS Start_date, DATE_FORMAT(End_date,'%Y-%m-%d') AS End_date FROM ballground_schedule_limited WHERE rand = ? LIMIT 1;";
		$data = func::excSQLwithParam('select', $sql_, array($rand), false, $PDOLink);
		
		$meter_mac = $data['meter_id'].','.$data['Address_MAC'];
		$st_date = $data['Start_date'];
		$ed_date = $data['End_date'];
		$mac = $data['Address_MAC'];
		if($updated) 
		{
			# 資料變動版號 + 1
			$sql = "SELECT ver FROM  ballground_reader_id_list where Address_MAC = ? LIMIT 1;";
			$data = func::excSQLwithParam('select', $sql, array($mac), false, $PDOLink);
			
			if($data['ver'])
			{
				$sql = "UPDATE ballground_reader_id_list SET ver = {$data['ver']}+1  WHERE Address_MAC =?";
				$updated_ver = func::excSQLwithParam('update', $sql, array($mac), false, $PDOLink);
				if($updated_ver)
				{
					$content = "[指定排程刪除]  日期: {$st_date}-{$ed_date}, rand: {$rand} 管理員:{$admin} ,ip:{$ip}, path:{$this_page}";
					func::toLog('mode', $content, $PDOLink);	 
					die(header("location: ../illumination_system_list.php?success=1&Ground_name=$Ground_name&sub_ground_name=$meter_mac&sch=ok"));				
				} 
			}else{
				die(header("location: ../illumination_system_list.php?error=2&Ground_name=$Ground_name&sub_ground_name=$meter_mac&sch=ok"));	
			}
		}		
	}

?>
 