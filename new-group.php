<?php 
include('header_layout.php');
include('nav.php');
if(!$_SESSION['admin_user']['id']) func::alertMsg('請登入', 'index.php', true);

	# 權限
	$user_sn = $_SESSION['admin_user']['sn'];
	$page = '群組權限管理'; 
	$normal = func::checkAuthorize($user_sn, $page, false, $PDOLink); // 後台 頁面權限
	if(!$normal) func::alertMsg('無此頁面權限', 'index.php', false);
	$sp = func::checkAuthorize($user_sn, '', true, $PDOLink); //  有sp權限
	// $sp = true;
	$display_sp = ($sp) ? 'block' : 'none';

	# 下拉選單
	# 帳號&群組
	$sql = "
	SELECT b.func_id, b.func_item AS item 
	FROM ao_menu_sub a
	INNER JOIN ao_function b ON a.sub_id = b.menu_sub_id
	WHERE b.func_item in ('帳號管理','群組權限管理') AND a.del_mark=0 AND b.del_mark=0;
	";
	$opt_ac_gup = func::excSQL($sql, $PDOLink, true);
	$opt_ac_gup_html = "";
	foreach($opt_ac_gup as $v){
		$opt_ac_gup_html .= "<option value='{$v['func_id']}'>{$v['item']}</option>";
	}
 
if($sp)
{
	# 金額設定 
	$sql = "
	SELECT b.func_id, CONCAT(c.area_name,'-',b.func_item) AS item 
	FROM ao_menu_sub a
	INNER JOIN ao_function b ON a.sub_id = b.menu_sub_id
	INNER JOIN ballground_area c ON b.area_id=c.id
	WHERE b.func_item in ('預設金額','排程金額') AND a.del_mark=0 AND b.del_mark=0;
	"; 
	$opt_price = func::excSQL($sql, $PDOLink, true);
	$opt_price_html = "";
	foreach($opt_price as $v){
		$opt_price_html .= "<option value='{$v['func_id']}'>{$v['item']}</option>";
	}
	
	# 模式設定
	$sql = "SELECT b.func_id, CONCAT(c.area_name,'-',b.func_item) AS item 
	FROM ao_menu_sub a
	INNER JOIN ao_function b ON a.sub_id = b.menu_sub_id
	INNER JOIN ballground_area c ON b.area_id=c.id
	WHERE b.func_item in ('模式切換','排程模式') AND a.del_mark=0 AND b.del_mark=0;
	";
	$opt_mode= func::excSQL($sql, $PDOLink, true);
	$opt_mode_html = "";
	foreach($opt_mode as $v){
		$opt_mode_html .= "<option value='{$v['func_id']}'>{$v['item']}</option>";
	}
}
 
?>

<section class="wrapper">
<div class="col-12 btn-back"><a href="roomgroup.php" ><i class="fas fa-chevron-circle-left fa-3x"></i><span class='back-font'><?php echo $lang->line("btn_back");?></span></a></div>

	<div class="row container-fluid mar-bot50 mar-center2">
		<?php if($_GET['success'] == 1){ ?>
			<div style="margin: 0 auto; text-align: center; width: 600px;" class="alert alert-success" role="alert">
			<strong>新增完成!!</strong> 
			</div>
		<?php } else if($_GET['error'] == 1){ ?>
			<div style="margin: 0 auto; text-align: center; width: 600px;" class="alert alert-danger" role="alert">
			<strong>新增失敗!! </strong> 
			</div>
		<?php } else if($_GET['error'] == 2){ ?>
			<div style="margin: 0 auto; text-align: center; width: 600px;" class="alert alert-danger" role="alert">
			<strong>新增失敗!! 群組名重複!! </strong> 
			</div>
		<?php } else if($_GET['error'] == 3){ ?>
			<div style="margin: 0 auto; text-align: center; width: 600px;" class="alert alert-danger" role="alert">
			<strong>新增失敗!! 請至少選取一種功能!! </strong> 
			</div>			
		<?php } ?> 
	</div>
 
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="mb-4 text-center">建立群組</h1>
				<form action='model/group_add.php' method='POST'>
					<div class="form-group row">
						<label class="col-md-12 col-lg-2 col-form-label label-right" >群組名稱</label>

						<div class="col-md-12 col-lg-9"> 
							<input   type="text" required='required' maxlength='10' class="form-control  col" name="groupname" placeholder="">
						</div>
					</div>
				<!-- 後台權限  -->
					<div class="form-group row">
						<label class="col-md-12 col-lg-2 col-form-label label-right pd-top25">後台權限</label>	
						<hr class='col-12 border-bottom-primary m-0 d-lg-none'>
						
						<div class="col-md-12 col-lg-9 form-inline">
							<label class="col-md col-lg col-form-label label-left pd-top25">帳號&群組</label>
							<select class="selectpicker form-control col-md-12 col-lg-10"  
							data-none-selected-text="請選擇" size="1" name="opt_ac_gup[]" multiple  >
								<?php echo $opt_ac_gup_html; ?> 
							</select>
						</div>
						</div> 

				<!--細項權限-->
					<div class="form-group row select-mar" style="display:<?php echo $display_sp;?>">
						<label class="col-md col-lg-2 col-form-label label-right pd-top25">細項權限</label>
						<hr class="col-12 border-bottom-primary m-0 d-lg-none">	
						<div class="col-lg-9 col-md-12 form-inline">
							<label class="col-md col-lg-2 col-form-label label-left pd-top25">金額設定</label>	
							<select class="selectpicker form-control col-md-12 col-lg-10 mb-1"  
							data-none-selected-text="請選擇" data-actions-box="true"  data-size="5"
							data-select-all-text='全選' data-deselect-all-text='取消全選' size="1"  name="sp_opt_price[]"   multiple >
								<?php echo $opt_price_html; ?>
 
							</select>
						</div>
						<div class="col-lg-9 offset-lg-2 col-md-12 form-inline select-mar3">
							<label class="col-md col-lg col-form-label label-left pd-top25">模式切換</label>	
							<select class="selectpicker form-control col-md-12 col-lg-10"  
							data-none-selected-text="請選擇" size="1"  name="sp_opt_mode[]" data-size="5" multiple data-select-all-text='全選' data-deselect-all-text='取消全選'  data-actions-box="true">
								<?php  echo $opt_mode_html; ?> 
							</select>
						</div>
					</div>
				<!--細項權限 END-->
					<div class="form-group row select-mar">
						<label  class="col-md-12 col-lg-2 col-form-label label-right">備註</label>	
						<div class="col-md-12 col-lg-9">
							<input type="text" maxlength='30' class="form-control" name="memo">
						</div>
					</div>
					<br>
					<button type="submit" class="btn  btn-loginfont btn-primary2  col-sm-6 offset-sm-3" onclick="return confirm('確認新增群組？');"><?php echo $lang->line("confirm_create");?></button><br>
				</form>
			</div>
		</div>
	</div>
</section>
<?php include('footer_layout.php'); ?>