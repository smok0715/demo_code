
<?php include('header_layout.php'); ?>
<?php include('nav.php'); ?>
<section class="wrapper">
    <!-- BTN返回 -->
    <div class="col-12 btn-back"><a href="roomlistgroup.php"><i class="fas fa-chevron-circle-left fa-3x"></i><span class='back-font'><?php echo $lang->line("btn_back");?></span></a></div>
    <div class="rwd-box"></div><br>
    <div class="container" style="text-align: center;">
        <h1 class='jumbotron-heading'><?php echo $lang->line("roomlist-manager"); ?></h1>
        <div class="row">
			<div class="col-lg col-sm-6 py-3 text-center"> 
				<a  href="new-member.php">
				<img class="mb-3" src="<?php echo $lang->line("iconimg1");?>">
				<h4>單筆新增</h4></a>
			</div>
			<div class="col-lg col-sm-6 py-3 text-center"> 
				<a  href="new-list.php">
				<img class="mb-3" src="<?php echo $lang->line("iconimg1"); ?>">
				<h4>名單匯入</h4> </a> 
			</div>
			<div class="col-lg col-sm-6 py-3 text-center">
				<a  href="new-editmember.php">
				<img class="mb-3" src="<?php echo $lang->line("iconimg1"); ?>">
				<h4>修改名單資料</h4> </a> 
			</div>
        </div>
    </div>
</section>
<?php include('footer_layout.php'); ?>

