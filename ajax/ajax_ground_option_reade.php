<?PHP 	 
include_once('../config/db.php');

# 區間查詢 下拉選單子查詢
$sel_area = $_POST['area']; 
$opt_data = array();    

if(isset($sel_area))
{
	if($sel_area == 'all' ) 
	{
		$sql = "
			SELECT  m.*, m.title FROM ballground_reader_id_list b
			LEFT JOIN ballground_machine m ON b.Address_MAC = m.MAC_address
			WHERE 1 ORDER BY m.title"; 
			$stmt  = $PDOLink->query($sql);
			$opt_data = $stmt->fetchAll();	
	}else if(is_numeric($sel_area)){
		$sql = "
			SELECT CONCAT(m.meter_id ,',', m.MAC_address) AS id_key, m.title
			FROM ballground_reader_id_list b
			LEFT JOIN ballground_machine m ON b.Address_MAC = m.MAC_address
			WHERE b.area = ? AND m.MAC_address <> '' ORDER BY m.title"; 
			$stmt  = $PDOLink->prepare($sql);
			$stmt -> execute(array($sel_area));
			$opt_data = $stmt->fetchAll();
	}
	try
	{ 
		if($opt_data)
		{
			$opt_txt = "
				<option value='unsel'>請選擇</option>
				<option value='all'>全選</option>
			";
			foreach($opt_data as $row){
				$opt_txt .="<option value={$row['id_key']}>{$row['title']}</opiton>";
			}
		} 
	}catch(PDOException $e) {
		echo $e->getMessage();
		exit();
	} 			
} 
//echo json_encode('call_back');
echo $opt_txt;
?>