<?php
    include('header_layout.php');
    include('nav.php');
	if(!isset($_SESSION['admin_user']['id'])) func::alertMsg('請登入', 'index.php' , true);  

	$pagesize = 18;
	$page	= 1;
    $ground_name = $_GET['Ground_name']; 
	$meter_mac = $_GET['sub_ground_name'] ;
	
if($_GET['sch'] == 'ok')
{
    $title = func::getGroundName($_GET['sub_ground_name'], $PDOLink); 
	$date_format = 'Y/m/d';
	$data  = array();
	$data_all = array();
	if($meter_mac)
	{
		$meter_mac_arr  = explode(',', $meter_mac);
		$meter = $meter_mac_arr[0];  
		$mac = $meter_mac_arr[1];   
		
		#分頁處理	 
		$sql = "
			SELECT schl.*, if((schl.rand%2 <> 0), CONCAT('week-',schl.weeknumber,'-',1),CONCAT('week-',schl.weeknumber,'-',2)) AS sign, 
			'0' AS del_makr, '' AS Start_date,'' AS End_date,'' AS p_start_day, '' AS p_end_day
			FROM ballground_schedule schl 
			WHERE schl.Address_MAC = :mac AND schl.meter_id = :meter
			UNION ALL
			SELECT schl_lim.*,
			DATE_FORMAT(schl_lim.Start_date,'%Y-%m-%d') AS p_start_day,  DATE_FORMAT(schl_lim.End_date,'%Y-%m-%d') AS p_end_day 
			FROM ballground_schedule_limited schl_lim
			WHERE schl_lim.Address_MAC = :mac AND schl_lim.meter_id = :meter AND del_mark = 0 ;	 
		";
		$stmt = $PDOLink->prepare($sql);
		$stmt -> execute(array(':mac'=>$mac, ':meter'=>$meter));
		$rownum =  $stmt -> rowCount(); 
		
		if(isset($_GET['page'])) {               
			$page = $_GET['page'];  
		} else {
			$page = 1;                                 
		} 
		$pageurl  = '';
		$pagenum  = (int) ceil($rownum / $pagesize);  
		$prepage  = $page - 1;                        
		$nextpage = $page + 1;
		
		if($page == 1) {                         
			$pageurl .= " ".$lang->line("index.home")." | ".$lang->line("index.previous_page")." | ";
		} else {
			$pageurl .= "<a href='?page=1&Ground_name={$ground_name}&sub_ground_name={$meter_mac}&sch=ok'>".$lang->line("index.home")."</a> | <a href='?page={$prepage}&Ground_name={$ground_name}&sub_ground_name={$meter_mac}&sch=ok'>".$lang->line("index.previous_page")."</a> | ";
		}
	
		if($page == $pagenum || $pagenum == 0) {     
			$pageurl .= " ".$lang->line("index.next_page")." | ".$lang->line("index.last_page")." ";
		} else {
			$pageurl .= "<a href='?page={$nextpage}&Ground_name={$ground_name}&sub_ground_name={$meter_mac}&sch=ok'>".$lang->line("index.next_page")."</a> | <a href='?page={$pagenum}&Ground_name={$ground_name}&sub_ground_name={$meter_mac}&sch=ok'>".$lang->line("index.last_page")."</a>";
		}	
	
		$sql = "
			SELECT schl.*, if((schl.rand%2 <> 0), CONCAT('week-',schl.weeknumber,'-',1),CONCAT('week-',schl.weeknumber,'-',2)) AS sign, 
			'0' AS del_makr, '' AS Start_date,'' AS End_date
			FROM ballground_schedule schl 
			WHERE schl.Address_MAC = :mac AND schl.meter_id = :meter	
			ORDER BY schl.rand ASC
		";
		$week_data = func::excSQLwithParam('select', $sql, array(':mac'=>$mac, ':meter'=>$meter) , true, $PDOLink);
		
		$sql = "
			SELECT schl_lim.*,
			DATE_FORMAT(schl_lim.Start_date,'%Y-%m-%d') AS p_start_day,  DATE_FORMAT(schl_lim.End_date,'%Y-%m-%d') AS p_end_day 
			FROM ballground_schedule_limited schl_lim 
			WHERE schl_lim.Address_MAC = :mac AND schl_lim.meter_id = :meter AND schl_lim.del_mark = 0 ORDER BY schl_lim.rand DESC	
		";
		$lim_data = func::excSQLwithParam('select', $sql, array(':mac'=>$mac, ':meter'=>$meter) , true, $PDOLink);
		
		foreach($week_data as $v) {  
			$data_all[] = $v;
		} 
		foreach($lim_data as $v) {
			$data_all[] = $v;
		} 
		$data = array();

		#分頁處理
		for($i = $prepage * $pagesize; ($i < $prepage * $pagesize + $pagesize) & ($i < $rownum); $i++) {
			$data[] = $data_all[$i];
		}				
	} 
}

?>
<section id="main" class="wrapper">
    <div class="col-12 btn-back"><a href="member.php"><i class="fas fa-chevron-circle-left fa-3x"></i><label class="previous"></label></a></div>
    <div class="rwd-box"></div><br><br>
    <div class="container" style="text-align: center;">
        <h1 class="jumbotron-heading text-center">時間排程設定</h1>
    </div>
    <div class="row container-fluid mar-center2">
        <?php if ($_GET['success'] == 2) { ?>
            <div style="margin: 0 auto; text-align: center;" class="alert alert-success col-lg-9" role="alert">
                <strong>【<?php echo $Title; ?>】 球場時間表已設定<br></strong>
            </div>
        <?php } elseif ($_GET['success'] == 1) { ?>
            <div style="margin: 0 auto; text-align: center;" class="alert alert-success col-lg-9" role="alert">
                <strong><?php echo "刪除成功！"; ?></strong>
            </div>			
        <?php } elseif ($_GET['error'] == 1) { ?>
            <div style="margin: 0 auto; text-align: center;" class="alert alert-danger col-lg-9" role="alert">
                <strong>【<?php echo $Title; ?>】<?php echo "無法更新！<br>請確認您準備設定的開始/結束時間：<br>1.是否超出該球場開放時段？<br>2.是否「結束時間」 小於 「開始時間」？"; ?></strong>
            </div>
        <?php } elseif ($_GET['error'] == 2) { ?>
            <div style="margin: 0 auto; text-align: center;" class="alert alert-danger col-lg-9" role="alert">
                <strong><?php echo "刪除失敗！"; ?></strong>
            </div>			
        <?php } ?>
    </div>
    <!--div class="h4 alert alert-orange col-10 mar-bot50 text-left">
        <p>製作說明:</p>
        <span class="text-orange pd-0">
            1.查詢結果：照舊，但記得百齡河濱公園」 拆成 「百齡左岸河濱公園」、「百齡右岸河濱公園」<br>
            2.如下表顯示各子球場的排程表<br>
            >其中表格只有星期一～日這七行要綠底，其餘皆為白底(如指定日期列)，換頁也是白底喔～<br>
            3.開始時間～結束時間：<br>
            >啟用時：才動態比對「開放時段」，若不符合時段中，則註記「超出開放時段！」；<br>
            >不啟用：則如下表顯示不啟用<br>
            <hr>
            4.排序方式：星期一～日，再來才是指定日期，依指定日期從最新日~舊往下排<br>
            5.<a href='' class='btn  text-orange' data-toggle="tooltip" data-placement="bottom" title='編輯'><i class='fas fa-pencil-alt'></i></a>
            ：星期一～日、新增的指定日期，皆可編輯相對應的排程設定<br>
            6.<a class='btn text-orange'  data-toggle="tooltip" data-placement="bottom"><i class='fas fa-trash-alt'></i></a>
            ：僅可刪除指定日期<br>
            <hr>
            7.新增指定日期：點擊後，進入相對應球場的新增指定日期設定；<br>EX.以下為例，點了新增指定日期後，會進入「百齡右岸河濱公園壘球場A」的新增指定日期頁<br>
            8.ver+1 備註除外
		</span> 
	</div-->
    <!-- SEARCH -->
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form id="form1" name="form1" action="illumination_system_list.php" method="get">
                        <div class="form-group row select-mar form-group-lg">
                            <label for="exampleFormControlInput1" class="col-sm-2 col-form-label label-right">球場名稱</label>
                            <div class="col-sm-9 form-inline">
                                <select class="room_changes col form-control" size="1" name="Ground_name" id="sch_area" >
                                    <option value="unsel" <?php if ($ground_name == 'unsel') print "selected"; ?>>請選擇</option>
                                    <?php echo func:: getAreaOption($PDOLink, $ground_name); ?>
                                </select>
                            </div>
                        </div>
                        <div class='form-group row form-group-lg div_meter' id = 'sub_block'>
                                <label for='exampleFormControlInput1' class='col-sm-2 col-form-label label-right'>子球場名稱</label>
                                    <div class='col-sm-9 form-inline'>
                                        <select class='room_changes col form-control' size='1' name='sub_ground_name' id='sch_sub_area'>
                                        </select>
                                    </div>
                        </div><br>
                        <div class="text-center offset-1">
							<input type="hidden" name="sch" value="ok">
                            <button type="button" onclick="export_list()" class="btn btn-loginfont btn-primary2 btn-mar2 col-4"><?php echo $lang->line("index.confirm_query2") ?></button>
                            <button type="button" onclick="reset1()" class="btn btn-loginfont btn-primary2 btn-mar2 col-4"><?php echo $lang->line("index.reset") ?></button>
							 
						</div>
                </form>
            </div>
        </div>
    </div>
    <!-- SEARCH END -->
    <!-- 當前子球場排程表 NEW -->
    <div class="container" 
			<?php  
				if($ground_name == 'unsel' ||  $meter_mac =='unsel' || $_GET['sch'] != 'ok')
				{
					$show_sch = "style=display:none";
				}else{
					$show_sch = "";
				}
				echo $show_sch; 
				?> >
        <div class="row">
            <div class="col-12">
                <h1 class='jumbotron-heading text-center h1-mar'><?php echo '排程設定：'.$title; ?></h1>
                <div class="text-right">
                    <button type="button" class="btn btn-loginfont btn-primary2 col-lg-3 col-md-4 offset-lg-9 mb-3" onclick="location.href='bgschedule-add.php<?php echo "?mac={$mac}&meter={$meter}";?>'">
                        新增指定日期
                    </button>
				</div>
                <div class="table-responsive">
                    <table class="table text-center"> 
                        <thead class="thead-green">
                            <tr>
                                <th scope="col">星期/指定日期</th>
                                <th scope="col">開始時間～結束時間</th>
                                <th scope="col">金額</th>
                                <th scope="col">模式</th>
                                <th scope="col">備註</th>
                                <th scope="col">操作</th>
                            </tr>
                        </thead>
                        <tbody class='schedule-setting'>
						<?php
						foreach($data as $v) {  
							if($v['weekNumber']){
								$week_title = '星期'.func::weekNumberChn($v['weekNumber']);
							}else{
								$week_title = date($date_format,strtotime($v['Start_date'])).'-'.date($date_format,strtotime($v['End_date']));
							} 							
								$id_key = explode("-",$v['sign']);
								$type = $id_key[0];  // 星期:week或 指定日期:priority
								$week = $id_key[1];  //星期:星期代號; 指定日期:key值
								$one_or_two = $id_key[2];  //排程1或2							
							?>
							<?php
							if($type == 'week')
							{
							   $time_txt = '';
							   $time_txt = func::scheduleTimeStatus($v['Address_MAC'], $v['meter_id'], $v['startTime'], $v['endTime'], $v['schedule_is_del'] , $PDOLink);
							   $status_mode = func::costModeChn($v['Status_mod']);
							   $red_text = '';
							   if(strpos($time_txt, '超出開放時段') !== false)  $red_text = "class='text_negative'" ;
							   
							   if ($one_or_two == "1" ) { // 排程1
                                    print "
									<tr border='0' style='background-color: #F0F7F7;'> 
										<td scope='row' rowspan='2'  style='border-bottom:2px solid #006666'>{$week_title}</td>
											<td scope='row' style='text-align:left; border-top:0px solid;border-bottom:0px solid;'>   
												<b class='b-label'>排程{$one_or_two}</b> 
												<span {$red_text}>{$time_txt}</span> 
											</td>
											<td scope='row' style='text-align:center;'>{$v['price']}</td>
											<td scope='row' style='text-align:center;'>{$status_mode}</td>
											<td scope='row' style='text-align:left;'>{$v['remark']}</td>
											<td scope='row' rowspan='2' style='border-bottom:2px solid #006666'>
											<a href='bgschedule-edit.php?meter={$v['meter_id']}&mac={$v['Address_MAC']}&type=week&weekday={$v['weekNumber']}&title={$title}' class='btn text-orange' data-toggle='tooltip' data-placement='bottom' title='編輯'><i class='fas fa-pencil-alt'></i></a>
										</td> 
									</tr>	
											";  
							   }else{ // 排程2
								   print "
								   <tr   border='0' style='border-top:0px solid; background-color: #F0F7F7;'>
										<td scope='row' style='text-align:left; border-top:0px solid; border-bottom:2px solid #006666;'  >   
											<b class='b-label' >排程{$one_or_two}</b> 
											<span {$red_text}>{$time_txt}</span>
										</td> 
										<td scope='row' style='text-align:center;border-top:0px solid;border-bottom:2px solid #006666;'>{$v['price']}</td>	
										<td scope='row' style='text-align:center;border-top:0px solid;border-bottom:2px solid #006666;' >{$status_mode}</td>		
										<td scope='row' style='text-align:left;border-top:0px solid;border-bottom:2px solid #006666;'>{$v['remark']}</td>
									</tr>	
								  ";   
							   }										
							}
							if($type == 'priority')
							{
							   $time_txt = '';
							   $time_txt = func::scheduleTimeStatus($v['Address_MAC'], $v['meter_id'], $v['startTime'], $v['endTime'], $v['schedule_is_del'] ,$PDOLink);
							   $status_mode = func::costModeChn($v['Status_mod']);
							   $red_text = '';
							   if(strpos($time_txt, '超出開放時段') !== false)  $red_text = "class='text_negative'" ;
							   if ($one_or_two == "1" ) { // 排程1
                                    print "
									<tr border='0'> 
										<td scope='row' rowspan='2'  style='border-bottom:2px solid #006666'>{$week_title}</td>
											<td scope='row' style='text-align:left; border-top:0px solid;border-bottom:0px solid;'>   
												<b class='b-label'>排程{$one_or_two}</b> 
												<span {$red_text}>{$time_txt}</span> 
											</td>
											<td scope='row' style='text-align:center;'>{$v['price']}</td>
											<td scope='row' style='text-align:center;'>{$status_mode}</td>
											<td scope='row' style='text-align:left;'>{$v['remark']}</td>
											<td scope='row' rowspan='2' style='border-bottom:2px solid #006666'> 
												<a href=bgschedule-edit.php?meter={$v['meter_id']}&mac={$v['Address_MAC']}&type=priority&rand={$v['rand']}&weekday={$v['weekNumber']}&title={$title}&st={$v['p_start_day']}&ed={$v['p_end_day']} class='btn text-orange' data-toggle='tooltip' data-placement='bottom' title='編輯'><i class='fas fa-pencil-alt'></i></a>
												<a   class='btn' onclick='del_schedule({$_GET['Ground_name']},{$v['rand']});' class='btn'  data-toggle='tooltip' data-placement='bottom'  title='刪除' data-original-title='刪除'><i class='fas fa-trash-alt  text-orange'></i></a>
										</td> 
									</tr>	
											";  
							   }else{ // 排程2
								   print "
								   <tr bgcolor='#F0F7F7' border='0' style='border-top:0px solid;'>
										<td scope='row' style='text-align:left; border-top:0px solid; border-bottom:2px solid #006666;'  >   
											<b class='b-label' >排程{$one_or_two}</b> 
											<span {$red_text}>{$time_txt}</span>
										</td> 
										<td scope='row' style='text-align:center;border-top:0px solid;border-bottom:2px solid #006666;'>{$v['price']}</td>	
										<td scope='row' style='text-align:center;border-top:0px solid;border-bottom:2px solid #006666;' >{$status_mode}</td>		
										<td scope='row' style='text-align:left;border-top:0px solid;border-bottom:2px solid #006666;'>{$v['remark']}</td>
									</tr>	
								  ";   
							   }									
							} 
						}
						?>			 
                        </tbody>
                    </table>
                    <!-- 跳頁 上下頁-->
                    <div class="container-fluid">
                        <div class="text-center">
                            <?php
                                if($rownum > $pagesize) {
                                    echo $pageurl;
                                    echo "".$lang->line("index.current_page")." $page | ".$lang->line("index.a_few_pages")." $pagenum ".$lang->line("index.page")."";
                                }
                            ?>
                        </div>
                    </div>
                    <!-- 跳頁 上下頁 end-->
                </div> 
            </div> 
        </div> 
    </div> 
    <!-- 當前子球場排程表 NEW END-->
</section>
<script>

$(document).ready(function() {
 
 $('#sub_block').hide();
		$("#sch_area").on("change", function () 	
		{
			var area_val = $('#sch_area').val() 
			if(area_val == 'unsel') 
			{	    
				$('#sub_block').hide();
			} else {
				$('#sub_block').show();
				ajaxGetSubOption('sch');
			}    

		}); 		
		if($("#sch_area").val() == 'unsel'){
				$('#sub_block').hide();
		}else{
				$('#sub_block').show();
				ajaxGetSubOption('sch');		
		}		
});
 
function export_list() {
    $('#form1').prop('action', 'illumination_system_list.php');
    $('#form1').prop('method', 'get');
    $('#form1').submit();
}
function reset1() {
    let url = window.location.href;
    url = url.split('?')[0];
    window.location = url;
}
function send_form() {
    let msg = "你確定要更新嗎?";
    if (confirm(msg) == true) {
        $('#form2').submit();
    } else {
        return false;
    }
}
function del_schedule(Ground_name, rand){
		if(confirm('確定刪除?')) {
	    location.replace("model/bgschedule-del_upd.php?Ground_name="+Ground_name+"&rand="+rand);
	} 
	
	return false;
}
 function ajaxGetSubOption(dom)
 {
	if(dom){
		let area = '#' + dom + '_area';
		let sub_area = '#' + dom + '_sub_area';
		$.ajax({
		method: "POST",
		data: { "area" : $(area).val() },
		url: "model/ajax_ground_option.php",
		dataType: "text"
		})
		.done(function(data) {
			//console.log(data);
			$(sub_area).empty(); 		 			 
			$(sub_area).append(data);
			$(sub_area).children().each(function(i,v){ 
				let sel_sub = GetURLParameter('sub_ground_name');			
				let decode = decodeURIComponent(decodeURIComponent(sel_sub)); 
				if(decode){
					if($(this).val() == decode)  $(this).attr("selected", "true");   //選取已選擇
				}			
			});
		});	
	}	
 }
 function GetURLParameter(param)
{
    let pageURL = window.location.search.substring(1);
    let sURLVariables = pageURL.split('&');
    for (let i = 0; i < sURLVariables.length; i++) 
    {
        let sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == param) 
        {
            return sParameterName[1];
        }
    }
}
 

</script>
<?php include('footer_layout.php'); ?>