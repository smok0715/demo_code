<?php
// $web_title = "test page"; 
ini_set("display_errors", false); 				
error_reporting(E_ALL^E_NOTICE^E_WARNING);

date_default_timezone_set("Asia/Taipei");
session_cache_expire(28800);					
ini_set('session.gc_probability',100); 

session_set_cookie_params(['samesite' => 'None']);
session_start();
ob_start();
ob_end_clean();

header("Cache-Control:no-cache,must-revalidate");
header("P3P: CP=".$_SERVER["HTTP_HOST"]."");     
header('Content-type: text/html; charset=utf-8');
header('Vary: Accept-Language');
 
?>