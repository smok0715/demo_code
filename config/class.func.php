<?php
include_once('Conn.php');	
/******************************************************************  
	Name   : func(v1.0)
	Creater:  chg -2020/12/23
	Modify :  chg -2022/01/21 - 刪除帶入PDO連線資料
*******************************************************************/
class func 
{ 
	public function __construct() 
	{  
		$DB = new Conn;  
		$this->pdo = $DB->db_conn() ; 
	}
	
	/******************************************************************
		Name   : excSQL - 執行sql語法 回傳搜尋資料 
		# sql_cmd無瀏覽器傳入資料才可使用
		Val    : 
				 @param  string      $sql_cmd  - sql語法
				 @param  boolean  $isAll  - true : fetchAll || false : fetch
				 @return  array  -    $data['data'] - 資料['欄位名']  
		Use    :  
		Creater: chg -2020/12/23
		Modify : chg -2021/01/08 - 加入參數 fetchAll 或 fetch 
	*******************************************************************/	
 	public function excSQL($sql_cmd, $isAll)
	{ 
		try{
			$stmt = null;
			$stmt = $this->pdo->prepare($sql_cmd);
			$stmt -> execute();
			($isAll) ? $data = $stmt -> fetchAll() : $data = $stmt -> fetch();
		}catch(PDOException $ex)
		{
			print $ex->getMessage();
			exit;
		}
		$this->pdo = null;
		return $data;
	}  
	

    /******************************************************************
		Name   : toLog - 紀錄操作資料至 log_list
		Val    : 
				 @param  string  $data_type - 模式變更-mode,停用-status,系統設定-system,登入-login
				 @param  string  $content     - 內文(自訂) 
		Creater: chg -2021/02/23
		Modify : 				  
	*******************************************************************/
	public function toLog($data_type, $content)
	{
		$sql="
			INSERT INTO log_list (data_type, content, add_date) 
			VALUES (:data_type, :content, now()); 
		";
		$param = array(
		":data_type" => $data_type,
		":content" => $content
		);	
		
		try {
			$stmt = $this->pdo->prepare($sql);
			$stmt -> execute($param);
			
		} catch(PDOException $e) {
			echo $sql . "<br>" . $e->getMessage();
			exit;
		}
		$this->pdo = null; 	
	}
	
    /******************************************************************
		Name   : getUserIP -  取得IP
		Val    : 
				 @return string  -連線IP
		Creater: chg -2021/01/11
		Modify : 				  
	*******************************************************************/
	public static function getUserIP()
	{
		$ip = "";
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else 
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}	
	
    /******************************************************************
		Name   : alertMsg - JS警告視窗
		Val    : 
				 @param  string  $msg   -  顯示文字
				 @param  string  $toPage -  導向的頁面
				 @param  boolean $jump -  是(true)||否(false)要導頁
				 @return   
		Creater: chg -2021/02/24
		Modify : 				  
	*******************************************************************/		
	public static function alertMsg($msg , $toPage, $jump)
	{
		if($msg)
		{
			if($jump)
			{
				echo  "<script type ='text/javascript'>alert('{$msg}'); ";
				echo "location.href = '{$toPage}'</script>";	
				exit;
			}else{
				echo  "<script type ='text/javascript'>alert('{$msg}'); </script>";
			}
		}
	}
 
    /******************************************************************
		Name   :  weekNumberChn  - 星期代號轉中文
		Val    :  
				 @param  string $weekday -  星期數字
				 @return  string  $week_day_chn - 星期中文
		Creater: chg -2021/03/30
		Modify : 				  
	*******************************************************************/			
	public static function weekNumberChn($weekday)
	{
		$week_day_chn = '';
		switch($weekday){
			case '1' :
				$week_day_chn = '一';
			break;
			case '2' :
				$week_day_chn = '二';
			break;
			case '3' :
				$week_day_chn = '三';
			break;
			case '4' :
				$week_day_chn = '四';
			break;
			case '5' :
				$week_day_chn = '五';
			break;
			case '6' :
				$week_day_chn = '六';
			break;		
			case '7' :
				$week_day_chn = '日';
			break;				
		}
		return $week_day_chn;
	}
 
    /******************************************************************
		Name   :  validateDate  - 是否為時間格式
		Val    :  
				 @param  string $date -   檢查的日期
				 @param  string $format -   比對格式 
				 @return  boolean  true || false
		Creater: chg -2021/04/21
		Modify : 				  
	*******************************************************************/		
	public static function validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	
 	/******************************************************************
		Name   : excSQLwithParam - 執行sql語法 或 回傳搜尋資料 
		Val    : 
				 @param  string 	   $type  -  sql動作	
				 @param  string      $sql_cmd  - sql條件
				 @param  array       $param_arr  - 帶入的參數陣列  
				 @param  boolean  $isAll  - true : fetchAll || false : fetch 		 
				 @return  boolean || array  $result 執行結果 || 搜尋資料  ( if fetch() $result['data'] - 資料['欄位名'])  
		Use    :  
		Creater : chg -2021/04/21
		Modify : 
	*******************************************************************/	
 	public  function excSQLwithParam($type, $sql_cmd, $param_arr, $isAll)
	{   
	    $stmt = null; 
		$result = null;
		switch($type)
		{
			case 'select':
				try{ 
					$stmt = $this->pdo->prepare($sql_cmd);
					$stmt -> execute($param_arr);
					($isAll) ? $result = $stmt -> fetchAll() : $result = $stmt -> fetch();
				}catch(PDOException $ex)
				{
					print '[select]'.$ex->getMessage();
					exit;
				} 
			break; 
			case 'insert':
			case 'update':
			case 'delete':
				$result = false;
				try{ 
					$stmt = $this->pdo->prepare($sql_cmd);
					$result = $stmt -> execute($param_arr); 
				}catch(PDOException $ex)
				{
					print '[exec]'.$ex->getMessage();
					exit;
				}			
			break;
		} 
		$this->pdo = null;
		return $result;
	} 
 	
}

?>