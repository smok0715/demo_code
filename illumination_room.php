<?php
    include_once('config/db.php');
	include('chk_log_in.php');
    include('header_layout.php');
    include('nav.php');

 
?>
<meta http-equiv="refresh" content="180">
<section id="main" class="wrapper">
    <div class="col-12 btn-back">
        <a href="illuminationRoomListPublic.php"><i class="fas fa-chevron-circle-left fa-3x"></i><label class="previous"></label></a>
    </div>
    <div class="rwd-box"></div><br><br>
    <div class="container" style="text-align: center;">
        <h1 class="jumbotron-heading text-center">華中河濱公園：球場系統使用現況</h1>
    </div>
    <div class="inner3 text-center">
        <!-- BTN刷新 -->
        <button type="button" onclick="window.location.reload();" class="btn btn-loginfont btn-primary2 btn-mar2" style="width:138px;">
            <i class="fas fa-redo-alt"></i>
        </button>
    </div>
    <br>
    <!-- 各球場使用狀態 -->
    <div class="inner">
        <div class="row justify-content-center ">
            <?php
                $NowDates = date("Y-m-d H:i:s");
                /* 球場裝置控制 */
                $sql = "SELECT * FROM ballground_machine WHERE title LIKE '華中河濱公園%' ORDER BY rand";
                $rs = $PDOLink->query($sql);
                $rs->setFetchMode(PDO::FETCH_ASSOC);
                $BallMachineStatusType = array(1 => '可使用', 2 => '使用中', '' => '可使用', 9 => '等待中');
                while ($ballground_machine_row = $rs->fetch()) {
                    $Reader_Id = $ballground_machine_row["reader_id"];
                    $MeterId = $ballground_machine_row["meter_id"];
					$Mac = $ballground_machine_row["MAC_address"];
                    $PowerStatus = $ballground_machine_row["power_status"];   
				print "<div class='col-lg-4 mb-4 '>
                               <div class='card card-h card-green text-green text-center h-100' >
                                   <div class='py-3'>
                                   <h1 class='m-0 font-weight-bold'>" . $ballground_machine_row["title"] . "</h1>
                                   </div>";  
                    $status = $rowWashinStatus["power_status"];  
					$status_chn = func::hardwareModeChn($PowerStatus);
 
					if ($status == 2  && $NowDates < $EndDate) 
					{
						$sec = 0;
						$html_id++;
						$sec = strtotime($EndDate) - strtotime($NowDates); 
					print "
						<span class='text-orange'>狀態：{$status_chn} </span>
						<div>
						   <input type='hidden' id ='hide_sec{$html_id}' value=$sec /> 
                            <i  id ='clock{$html_id}' class='far fa-clock'></i><span id = label{$html_id}>&nbsp;剩餘時間&nbsp;</span>
                            <span id = 'time{$html_id}'></span>
                        </div>"; 				
					} else { 
						print "
						<span class='text-orange'>狀態：{$status_chn}</span>"; 
					}
		 
                    print "</div>
                    </div>";
                }
            ?>
        </div>
    </div>
</section>
<script>
 
$(document).ready(function() {
	var sec1 = $("#hide_sec1").val();
	var sec2 = $("#hide_sec2").val();

    var counter = window.setInterval(() =>{
    if(sec1 > 0){
		TimeCount('#time1', sec1); 	
		sec1--;
    }else{
		$('#time1').hide(); $('#clock1').hide(); $('#label1').hide();
	}
    if(sec2 > 0){
		TimeCount('#time2', sec2); 
		sec2--;  
    }else{
		$('#time2').hide(); $('#clock2').hide(); $('#label2').hide();
	}
   },1000);
   });
	
	function TimeCount(id, sec) 
	{  
		let hour = Math.floor(sec / 3600);
		let minute = Math.floor(Math.floor(sec % 3600) / 60);
		let second = sec % 60;
		if(minute < 10) minute = '0' + minute;
		if(second < 10) second = '0' + second;
		let count  = hour + "小時" + minute + "分" + second + "秒";  
		$(id).text(count);
	}
  
</script>
<?php include('footer_layout.php'); ?>